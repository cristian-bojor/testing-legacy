package work.testing;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import work.testing.BookReader.ReadResult;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

/**
 * @author fbojor on 23.03.2017.
 */
@Test
public class BookReaderTest {

    private static final String NAME_ERRORS_KEY = "name";
    private static final String RELEASE_DATE_ERRORS_KEY = "releaseDate";
    private static final String VERSION_ERRORS_KEY = "version";

    private BookReader bookReader;

    @BeforeMethod
    public void setUp() throws Exception {
        bookReader = new BookReader();
    }

    @Test(dataProvider = "validLinesToExpectedBooks", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_validLines_expectedBooksAndNoErrors(List<String> lines, List<Book> expectedBooks) {
        ReadResult result = bookReader.readBooks(lines);

        assertNotNull(result.getBooks());
        assertEquals(result.getBooks(), expectedBooks);

        result.getErrors().values()
                .forEach(list -> assertTrue(list.isEmpty()));
    }

    public void readBooks_emptyList_emptyResult() {
        ReadResult result = bookReader.readBooks(new ArrayList<>());

        assertNotNull(result);

        assertNotNull(result.getBooks());
        assertTrue(result.getBooks().isEmpty());

        assertNotNull(result.getErrors());
        result.getErrors().values()
                .forEach(list -> assertTrue(list.isEmpty()));
    }

    @Test(dataProvider = "emptyNameLineToError", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_emptyNameLines_nameEmptyErrors(List<String> lines, List<String> expectedErrors) {
        ReadResult result = bookReader.readBooks(lines);
        assertContainsErrorsAtKey(result, NAME_ERRORS_KEY, expectedErrors);
    }

    @Test(dataProvider = "invalidNameLineToError", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_invalidNameLines_nameInvalidErrors(List<String> lines, List<String> expectedErrors) {
        ReadResult result = bookReader.readBooks(lines);
        assertContainsErrorsAtKey(result, NAME_ERRORS_KEY, expectedErrors);
    }

    @Test(dataProvider = "emptyReleaseDateLineToError", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_emptyReleaseDateLines_releaseDateEmptyErrors(List<String> lines,
                                                                       List<String> expectedErrors) {
        ReadResult result = bookReader.readBooks(lines);
        assertContainsErrorsAtKey(result, RELEASE_DATE_ERRORS_KEY, expectedErrors);
    }

    @Test(dataProvider = "invalidReleaseDateLineToError", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_invalidReleaseDateLines_releaseDateEmptyErrors(List<String> lines,
                                                                       List<String> expectedErrors) {
        ReadResult result = bookReader.readBooks(lines);
        assertContainsErrorsAtKey(result, RELEASE_DATE_ERRORS_KEY, expectedErrors);
    }

    @Test(dataProvider = "invalidVersionLinesToError", dataProviderClass = BookLinesDataProvider.class)
    public void readBooks_invalidVersion_versionInvalidErrors(List<String> lines,
                                                              List<String> expectedErrors) {
        ReadResult result = bookReader.readBooks(lines);
        assertContainsErrorsAtKey(result, VERSION_ERRORS_KEY, expectedErrors);
    }

    private void assertContainsErrorsAtKey(ReadResult result, String key, List<String> expectedErrors) {
        assertNotNull(result.getErrors());
        List<String> keyErrors = result.getErrors().get(key);

        assertNotNull(keyErrors);
        assertEquals(keyErrors, expectedErrors);
    }

}
