package work.testing;

import org.testng.annotations.DataProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author fbojor on 29.03.2017.
 */
public final class BookLinesDataProvider {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private BookLinesDataProvider() {
        throw new UnsupportedOperationException();
    }

    @DataProvider
    public static Object[][] validLinesToExpectedBooks() throws ParseException {
        List<Book> books = Arrays.asList(
                Book.builder()
                        .name("Aname1")
                        .releaseDate(getDate("1995-10-22"))
                        .version("01")
                        .group("")
                        .build(),
                Book.builder()
                        .name("Z")
                        .releaseDate(getDate("2010-10-22"))
                        .version("12")
                        .group("424sad")
                        .build(),
                Book.builder()
                        .name("Test")
                        .releaseDate(getDate("2014-01-02"))
                        .version("01")
                        .group("ss--ss")
                        .build()
        );

        List<String> lines = getLines(books);

        return new Object[][]{
                {lines, books}
        };
    }

    @DataProvider
    public static Object[][] emptyNameLineToError() {
        List<String> lines = Arrays.asList(
                ",12052012,22,asd",
                ",12052002,12411,asd",
                ",12012012,,asd",
                ",12012012,52sd,a24",
                ",,,"
        );


        List<String> errors = IntStream.range(0, lines.size())
                .mapToObj(lineNr -> String.format("Name is empty on line %d", lineNr))
                .collect(Collectors.toList());

        return new Object[][]{
                {lines, errors}
        };
    }

    @DataProvider
    public static Object[][] invalidNameLineToError() {
        List<String> lines = Arrays.asList(
                " sdas,12052012,22,asd",
                "aaa,12052002,12411,asd",
                "ss,12012012,,asd",
                "24,12012012,52sd,a24",
                "12sd,,,"
        );

        List<String> errors = IntStream.range(0, lines.size())
                .mapToObj(lineNr -> String.format("Name one line %d should start with a big letter", lineNr))
                .collect(Collectors.toList());


        return new Object[][]{
                {lines, errors}
        };
    }

    @DataProvider
    public static Object[][] emptyReleaseDateLineToError() {
        List<String> lines = Arrays.asList(
                "asd,,123,asd",
                "asd,,232,asd",
                "ffs,,23,asd",
                "ffsaf2,,22,a24",
                ",,,"
        );

        List<String> errors = IntStream.range(0, lines.size())
                .mapToObj(lineNr -> String.format("Release date is empty on line %d", lineNr))
                .collect(Collectors.toList());

        return new Object[][]{
                {lines, errors}
        };
    }

    @DataProvider
    public static Object[][] invalidReleaseDateLineToError() {
        List<String> lines = Arrays.asList(
                "asd,202210315-22,123,asd",
                "asd,20221050250,232,asd",
                "ffs,2010-2102455,23,asd",
                "ffsaf2,1232,22,a24",
                ",2424242,,"
        );

        List<String> errors = IntStream.range(0, lines.size())
                .mapToObj(lineNr -> String.format("Invalid date format on line %d", lineNr))
                .collect(Collectors.toList());

        return new Object[][]{
                {lines, errors}
        };
    }

    @DataProvider
    public static Object[][] invalidVersionLinesToError() {
        List<String> lines = Arrays.asList(
                "asd,,123,asd",
                "asd,,232,asd",
                "ffs,,223,asd",
                "ffsaf2,,242,a24",
                ",,2421,2422"
        );

        List<String> errors = IntStream.range(0, lines.size())
                .mapToObj(lineNr -> String.format("Version is invalid on line %d", lineNr))
                .collect(Collectors.toList());

        return new Object[][]{
                {lines, errors}
        };
    }

    private static List<String> getLines(List<Book> books) {
        return books.stream()
                .map(BookLinesDataProvider::toCsvLine)
                .collect(Collectors.toList());
    }

    private static String toCsvLine(Book book) {
        return String.join(",", new String[]{
                book.getName(),
                DATE_FORMAT.format(book.getReleaseDate()),
                book.getVersion(),
                book.getGroup()
        });
    }

    private static Date getDate(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.parse(date);
    }
}
