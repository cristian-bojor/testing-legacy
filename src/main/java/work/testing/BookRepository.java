package work.testing;

/**
 * @author fbojor on 20.03.2017.
 */
public interface BookRepository {
    void save(Book book);
}
