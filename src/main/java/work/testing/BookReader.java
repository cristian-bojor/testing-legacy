package work.testing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Character.isUpperCase;

/**
 * @author fbojor on 20.03.2017.
 */
public class BookReader {

    public static final int NAME_POSITION = 0;
    public static final int RELEASE_DATE_POSITION = 1;
    public static final int VERSION_POSITION = 2;
    public static final int GROUP_POSITION = 3;

    public static final String NAME_ERRORS_KEY = "name";
    public static final String RELEASE_DATE_ERROR_KEY = "releaseDate";
    public static final String VERSION_ERROR_KEY = "version";
    public static final String GROUP_ERROR_KEY = "group";

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public ReadResult readBooks(List<String> fileLines) {
        List<ReadBookResult> readBookResults = IntStream.range(0, fileLines.size())
                .mapToObj(index -> readBook(fileLines.get(index), index))
                .collect(Collectors.toList());

        return collectToReadResult(readBookResults);
    }

    private ReadBookResult readBook(String fileLine, int lineNumber) {
        BookFields bookFields = readBookFields(fileLine);

        Map<String, List<String>> errors = validateBookFields(bookFields, lineNumber);
        Book book = toBook(bookFields, errors);
        return new ReadBookResult(book, errors);
    }

    private Book toBook(BookFields bookFields, Map<String, List<String>> errors) {
        Book book = new Book();
        if (errors.get(NAME_ERRORS_KEY).isEmpty()) {
            book.setName(bookFields.getName());
        }
        if (errors.get(RELEASE_DATE_ERROR_KEY).isEmpty()) {
            book.setName(bookFields.getName());
        }
        if (errors.get(VERSION_ERROR_KEY).isEmpty()) {
            book.setName(bookFields.getName());
        }
        if (errors.get(GROUP_ERROR_KEY).isEmpty()) {
            book.setName(bookFields.getName()) ;
        }
        return book;
    }

    private Map<String, List<String>> validateBookFields(BookFields bookFields, int lineNumber) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put(NAME_ERRORS_KEY, validateName(bookFields.getName(), lineNumber));
        errors.put(RELEASE_DATE_ERROR_KEY, validateReleaseDate(bookFields.getReleaseDate(), lineNumber));
        errors.put(VERSION_ERROR_KEY, validateVersion(bookFields.getVersion(), lineNumber));
        errors.put(GROUP_ERROR_KEY, validateGroup(bookFields.getGroup(), lineNumber));
        return errors;
    }

    private List<String> validateGroup(String group, int lineNumber) {
        return new ArrayList<>();
    }

    private List<String> validateVersion(String version, int lineNumber) {
        List<String> errors = new ArrayList<>();
        if (version.length() > 2) {
            errors.add("Version is invalid on line " + lineNumber);
        }
        return errors;
    }

    private List<String> validateReleaseDate(String releaseDate, int lineNumber) {
        List<String> errors = new ArrayList<>();
        if (releaseDate.length() == 0) {
            errors.add("Release is empty on line " + lineNumber);
        } else if (!hasValidFormat(releaseDate)) {
            errors.add("Invalid date format on line " + lineNumber);
        }
        return errors;
    }

    private boolean hasValidFormat(String releaseDate) {
        try {
            DATE_FORMAT.parse(releaseDate);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private List<String> validateName(String name, int lineNumber) {
        List<String> errors = new ArrayList<>();
        if (name.length() == 0) {
            errors.add("Name is empty on line " + lineNumber);
        } else if (!isUpperCase(name.charAt(0))) {
            errors.add("Name one line " + lineNumber + " should start with a big letter");
        }
        return errors;
    }

    private BookFields readBookFields(String fileLine) {
        String[] fields = fileLine.split(",", -1);
        return BookFields.builder()
                .name(fields[NAME_POSITION])
                .releaseDate(fields[RELEASE_DATE_POSITION])
                .version(fields[VERSION_POSITION])
                .group(fields[GROUP_POSITION])
                .build();
    }

    private ReadResult collectToReadResult(List<ReadBookResult> readBookResults) {
        List<Book> books = new ArrayList<>();

        Map<String, List<String>> errors = new HashMap<>();
        for (ReadBookResult readBookResult : readBookResults) {
            books.add(readBookResult.getBook());

            addErrors(readBookResult, errors);
        }

        return new ReadResult(books, errors);
    }

    private void addErrors(ReadBookResult readBookResult, Map<String, List<String>> errors) {
        readBookResult.errors.forEach((key, errorsList) -> this.addErrors(key, errorsList, errors));
    }

    private void addErrors(String key, List<String> errorsList, Map<String, List<String>> errors) {
        errors.putIfAbsent(key, new ArrayList<>());
        errors.get(key).addAll(errorsList);
    }

    @Getter
    @AllArgsConstructor
    static class ReadResult {
        List<Book> books;
        Map<String, List<String>> errors;
    }

    @Getter
    @AllArgsConstructor
    private static class ReadBookResult {
        Book book;
        Map<String, List<String>> errors;
    }

    @Getter
    @Builder
    private static class BookFields {
        String name;
        String releaseDate;
        String version;
        String group;
    }
}
